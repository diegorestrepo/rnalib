#include"RNAlib.h"

float cuadrado(float n)
{
	return pow(n,2);
}

float devcuadrado(float n)
{
	return 2*n;
}

int main(void)
{
	int i=0,j=0,cont=0;
	float u=0.05;

	float T[2]={-5,-10};
	float P[2]={1,2};
	float W[2]={2.01,0.0};
	float b[2]={0.0,0.01};

	float n1[2]={0,0};
	float a1[2]={0,0};
	float a2[2]={0,0};
	float e[2]={0,0};
	float emc = 1;

	float S[2][2] = {{0,0},{0,0}};
	float **mS;

	float y[2][4]={};
	float **my;
	float x[2][4]={};

	float J[2][4]={};
	float **mJ;

	float *ve;

	float dx[4]={};
	float *vdx;

	float xwb[4]={};
	float *vxwb;

	float nx[4]={};
	float *vnx;

	FILE *epocas;
	FILE *pesos_bias;
	FILE *pesos_bias_epocas;

	epocas = fopen("epocas.dat","w");
	pesos_bias = fopen("pesos_bias.dat","w");
	pesos_bias_epocas = fopen("pesos_bias_epocas.dat","w");

	ve = vector(1,2);
	ve = e;

	vdx = vector(1,4);
	vdx = dx;

	vxwb = vector(1,4);
	vxwb = xwb;

	vnx = vector(1,4);
	vnx = nx;

	do
	{
		printf("\n\nIteración número: %d\n\n",cont);
		//Propagación
		if(cont == 0)
		{
			for(i=0;i<2;i++)
			{
				n1[i] = neurona_1in_1out(P[i],W[0],b[0],linear);
				a1[i] = cuadrado(n1[i]);
				a2[i] = neurona_1in_1out(a1[i],W[1],b[1],linear);
				e[i] = error(T[i],a2[i]);
				printf("\nCuando P=%f a es %f\n",P[i],a2[i]);
				printf("Cuando T=%f y a=%f, el error es %f\n",T[i],a2[i],e[i]);
			}
		}
		else
		{
			W[0] = nx[0];
			b[0] = nx[1];
			W[1] = nx[2];
			b[1] = nx[3];

			for(i=0;i<2;i++)
			{
				n1[i] = neurona_1in_1out(P[i],W[0],b[0],linear);
				a1[i] = cuadrado(n1[i]);
				a2[i] = neurona_1in_1out(a1[i],W[1],b[1],linear);
				e[i] = error(T[i],a2[i]);
				printf("\nCuando P=%f a es %f\n",P[i],a2[i]);
				printf("Cuando T=%f y a=%f, el error es %f\n",T[i],a2[i],e[i]);
			}
		}

		emc = mse(&T[0],&a2[0],2);
		printf("\nEl Error Medio Cuadrado es %f\n",emc);
		fprintf(epocas,"%d %f\n",cont,emc);

		//Sensivilidad de Marquartd
		printf("\nSensitivities\n");
		mS=matriz(1,2,1,2);

		for(i=0;i<2;i++)
		{
			mS[i]=S[i];
		}

		for(i=0;i<2;i++)
		{
			mS[1][i] = -1;
			mS[0][i] = devcuadrado(n1[i])*W[1]*mS[1][i];
		}

		imprimirmatriz(mS,2,2);

		my=matriz(1,2,1,4);
		for(i=0;i<2;i++)
		{
			my[i]=y[i];
		}

		sensitive2Y(mS,2,2,2,my);
		printf("\n Y \n");
		imprimirmatriz(my,2,4);

		x[0][0]=P[0];
		x[1][0]=P[1];

		x[0][1]=1;
		x[1][1]=1;

		x[0][2]=a1[0];
		x[1][2]=a1[1];

		x[0][3]=1;
		x[1][3]=1;

		mJ = matriz(1,2,1,4);
		for(i=0;i<2;i++)
		{
			for(j=0;j<4;j++)
			{
				J[i][j]=my[i][j]*x[i][j];
			}

			mJ[i] = J[i];
		}

		printf("\nJacobiano\n");
		imprimirmatriz(mJ,2,4);

		func_lm(mJ,ve,u,vdx);
		printf("\nSalida LM\n");
		imprimirvector(vdx,4,0);

		//Actualizar pesos y bias
		xwb[0] = W[0];
		xwb[1] = b[0];
		xwb[2] = W[1];
		xwb[3] = b[1];

		printf("\nx-dx\n");
		invertirsignovector(vdx,4);
		sumarvectores(vxwb,vdx,4,vnx);
		imprimirvector(vnx,4,0);
		fprintf(pesos_bias_epocas,"%d %f %f %f %f\n",cont, vnx[0], vnx[1], vnx[2], vnx[3]);

		++cont;

	}while(emc > 0.000021);

	for(i=0;i<4;i++)
	{
		fprintf(pesos_bias,"%f\n",vnx[i]);
	}

	return 0;
}
