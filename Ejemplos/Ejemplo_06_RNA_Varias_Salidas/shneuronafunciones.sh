#!/bin/bash

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)

echo "$AZUL Verificar RNAlib $LIMPIAR"
if [ -f RNAlib.h ];
then
	echo "$VERDE RNAlib.h se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.h no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.h fué llamado al directorio $LIMPIAR"
	echo
	ln -s ../../librerias/RNAlib.h
fi
if [ -f RNAlib.c ];
then
	echo "$VERDE RNAlib.c se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.c no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.c fué llamado al directorio $LIMPIAR"
	echo
	ln -s ../../librerias/RNAlib.c
fi

echo
echo "$VERDE Se nuestran los efectos de las funciones de activación vistas en el capitulo en una neurona con dos entradas $LIMPIAR"
echo

echo
echo "$AZUL ============================================================================== $LIMPIAR"
echo "$AZUL =                             Ingrese la entrada P1                          = $LIMPIAR"
echo "$AZUL ============================================================================== $LIMPIAR"
echo
read P1

echo
echo "$AZUL ============================================================================== $LIMPIAR"
echo "$AZUL =                             Ingrese la entrada P2                          = $LIMPIAR"
echo "$AZUL ============================================================================== $LIMPIAR"
echo
read P2

echo
echo "$AZUL ============================================================================== $LIMPIAR"
echo "$AZUL =                             Ingrese el peso W1                             = $LIMPIAR"
echo "$AZUL ============================================================================== $LIMPIAR"
echo
read W1

echo
echo "$AZUL ============================================================================== $LIMPIAR"
echo "$AZUL =                             Ingrese el peso W2                             = $LIMPIAR"
echo "$AZUL ============================================================================== $LIMPIAR"
echo
read W2

echo
echo "$AZUL ============================================================================== $LIMPIAR"
echo "$AZUL =                             Ingrese el bias b                              = $LIMPIAR"
echo "$AZUL ============================================================================== $LIMPIAR"
echo
read b

#For para varias las funciones de activación
for funcion in hardlim hardlims linear logsig tansig
do

cat > neurona$funcion.c << EOF
#include"RNAlib.h"

int main(void)
{
	//Entrada (Estimulo de la RNA)
	float P1=$P1, P2=$P2;

	//Pesos sinapticos
	float W1=$W1, W2=$W2;

	//Bias
	float b=$b;

	//Salida
	float a=0.0;

        a = neurona_2in_1out(P1,P2,W1,W2,b,$funcion);

	printf("la salida es: %f\n\n",a);

	return 0;
}
EOF

gcc -Wall -o ejecutar neurona$funcion.c RNAlib.c -lm
echo
echo "$LILA Ejecución con la función: $ROJO $funcion $LIMPIAR"
./ejecutar
echo
rm ejecutar neurona$funcion.c

done

exit 0
