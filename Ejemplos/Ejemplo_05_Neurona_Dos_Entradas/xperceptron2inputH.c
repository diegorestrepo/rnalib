#include"RNAlib.h"

int main(void)
{
	//Entrada (Estimulo de la RNA)
	float P1=0.0, P2=0.0;

	//Pesos sinapticos
	float W1=0.0, W2=0.0;

	//Bias
	float b=0.0;

	//Salida
	float a=0;

	printf("\nIngrese las entradas P1 y P2\n");
	scanf("%f",&P1);
	scanf("%f",&P2);

	printf("Ingrese los pesos sinapticos W1 y W2\n");
	scanf("%f",&W1);
	scanf("%f",&W2);

	printf("Ingrese el bias b\n");
	scanf("%f",&b);

    a = neurona_2in_1out(P1,P2,W1,W2,b,hardlims);

	printf("\nCuando P es [%f %f],\n",P1,P2);
	printf("W es [%f %f] y \n",W1,W2);
	printf("b es %f,\n",b);
	printf("la salida es: %d\n\n",(int)a);

	return 0;
}
