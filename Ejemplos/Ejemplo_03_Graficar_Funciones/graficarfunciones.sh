#!/bin/bash

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)


GP_COMMAND=`which gnuplot 2>/dev/null`
if [ "$GP_COMMAND" = "" ];
then
         echo
         echo "gnuplot no está en el PATH"
         echo "Los resultados no pueden ser graficados"
fi

echo "$AZUL Verificar RNAlib $LIMPIAR"
if [ -f RNAlib.h ];
then
	echo "$VERDE RNAlib.h se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.h no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.h fue llamado al directorio $LIMPIAR"
	echo
	ln -s ../../librerias/RNAlib.h
fi
if [ -f RNAlib.c ];
then
	echo "$VERDE RNAlib.c se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.c no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.c fue llamado al directorio $LIMPIAR"
	echo
	ln -s ../../librerias/RNAlib.c
fi

#For para varias las funciones de activación
for funcion in linear logsig tansig
do

cat > graficarfunciones.c << EOF
#include"RNAlib.h"

int main(void)
{
	float n=-6;
	float a=0.0;
	int i=0;

	FILE *grafica;

	grafica=fopen("grafica$funcion.dat","w");

	for(i=0;i<120;++i)
	{
		a=$funcion(n);
        	fprintf(grafica,"%f  %f\n",n,a);
		n+=0.1;
	}

	return 0;
}
EOF

echo "$AZUL Compilar $LIMPIAR"
gcc -Wall -o ejecutar graficarfunciones.c RNAlib.c -lm
echo

echo "$VERDE EJECUTAR $LIMPIAR"
./ejecutar
echo

echo "$ROJO Eliminar archivos $LIMPIAR"
rm ejecutar graficarfunciones.c
echo

done


if [ "$GP_COMMAND" = "" ];
then
    break
else
cat > gnuplot.tmp <<EOF
set encoding iso_8859_15
set terminal postscript enhanced solid color "Helvetica" 20
set output "graficatansig.pdf"
#
set key off

set   autoscale
unset log
unset label
set xtic auto
set ytic auto
set grid
plot "graficatansig.dat" u 1:2 title 'Tansig' w l lw 1.5 lc rgb "red"
EOF
echo "$AZUL Creando pdf $LIMPIAR"
$GP_COMMAND gnuplot.tmp
echo "$VERDE Listo $LIMPIAR"
fi

if [ "$GP_COMMAND" = "" ];
then
    break
else
cat > gnuplot.tmp <<EOF
set encoding iso_8859_15
set terminal postscript enhanced solid color "Helvetica" 20
set output "graficalogsig.pdf"
#
set key off

set   autoscale
unset log
unset label
set xtic auto
set ytic auto
set grid
plot "graficalogsig.dat" u 1:2 title 'Logsig' w l lw 1.5 lc rgb "red"
EOF
echo "$AZUL Creando pdf $LIMPIAR"
$GP_COMMAND gnuplot.tmp
echo "$VERDE Listo $LIMPIAR"
fi

if [ "$GP_COMMAND" = "" ];
then
    break
else
cat > gnuplot.tmp <<EOF
set encoding iso_8859_15
set terminal postscript enhanced solid color "Helvetica" 20
set output "graficalinear.pdf"
#
set key off

set   autoscale
unset log
unset label
set xtic auto
set ytic auto
set grid
plot "graficalinear.dat" u 1:2 title 'Linear' w l lw 1.5 lc rgb "red"
EOF
echo "$AZUL Creando pdf $LIMPIAR"
$GP_COMMAND gnuplot.tmp
echo "$VERDE Listo $LIMPIAR"
fi

echo "$ROJO Eliminar archivos $LIMPIAR"
#rm *.dat *.tmp
echo "$VERDE Listo $LIMPIAR"

exit 0
