#!/bin/bash

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)

echo "$AZUL Verificar RNAlib $LIMPIAR"
if [ -f RNAlib.h ];
then
	echo "$VERDE RNAlib.h se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.h no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.h fue llamado al directorio $LIMPIAR"
	echo
	ln -s ../../../librerias/RNAlib.h
fi
if [ -f RNAlib.c ];
then
	echo "$VERDE RNAlib.c se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.c no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.c fue llamado al directorio $LIMPIAR"
	echo
	ln -s ../../../librerias/RNAlib.c
fi

cat > funciones_hardlim.c << EOF
#include"RNAlib.h"

int main(void)
{
	//Entrada (Estimulo de la RNA)
	float n=0.0;

	//Salida de cada una de las funciones
	int a_hardlim=0.0;
	int a_hardlims=0.0;

	printf("Ingrese un número decimal (estímulo de la neurona)\n");
	scanf("%f",&n);

	a_hardlim=hardlim(n);
	a_hardlims=hardlims(n);

	printf("Cuando n es %f la salida de linear es: %d\n",n,a_hardlim);
	printf("Cuando n es %f la salida de logsig es: %d\n",n,a_hardlims);

	return 0;
}
EOF

gcc -Wall -o ejecutar_hardlim funciones_hardlim.c RNAlib.c -lm
./ejecutar_hardlim
rm ejecutar_hardlim funciones_hardlim.c

exit 0
