'''
 * ------------------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
 * ------------------------------------------------------------------------------------
'''

import numpy as np
from RNAlib import*


def run():
    W1 = np.array([[8.4204, 8.0895], [-9.2747, -9.6075]])
    P1 = np.array([[0], [0]])
    b1 = np.array([[-12.5021], [4.1393]])

    W2 = np.array([[-19.5154, -23.3266]])
    b2 = np.array([11.1044])

    rna = FeedforWard(P1=P1, W1=W1, b1=b1, function1="logsig",
                      W2=W2, b2=b2, function2="logsig")
    a = rna.MLP()
    print(a)


if __name__ == '__main__':
    run()
