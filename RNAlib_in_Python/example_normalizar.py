'''
 * ------------------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
 * ------------------------------------------------------------------------------------
'''

from RNAlib import *


def _normalizar():
    print("Incrementar en 1 y normalizar:")

    x = np.array([1])
    xmax = 10
    xmin = 1

    finc = 10

    for i in range(finc):
        xn = normalizar(x, xmax, xmin, 0)

        print(x, xn)

        x += 1


def _normalizar_array():
    print("Crear array y normalizar")

    x = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

    normalizara = normalizar_array(normalizar)
    xn = normalizara(x, 0)

    print(x, xn)


def run():
    _normalizar()
    _normalizar_array()


if __name__ == '__main__':
    run()
